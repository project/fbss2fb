
The Facebook-Style Statuses to Facebook module is a small module that allows
users to post their Facebook-Style Statuses
(http://drupal.org/project/facebook_status) to Facebook automatically on
updates. This module requires the Drupal for Facebook connect submodule which
links local Drupal profiles with users' Facebook accounts.
(http://drupal.org/project/fb).

Installation Instructions: Download to sites/%/modules and enable.

Note: The installation of dependency modules for the Drupal for Facebook
project (http://drupal.org/project/fb) has lengthy installation instructions
that must be accomplished first. They include installing third party Facebook
API's that are not packaged on Drupal.org. Reference the README file in the
Drupal for Facebook module for the most up to date installation instructions.

You will be required to register your app (Drupal website) with Facebook.
Instructions can be found at http://www.facebook.com/developers/.

In addition, the FB Extended Permissions module is required (included as a submodule of Drupal for Facebook).  You will need to ensure the 'Publish to my Wall' permission is checked under your app settings in Drupal.

Default settings are available at admin/settings/facebook_status. Ensure users
have permission to post to Facebook. When they first connect, they will be
asked to authorize your Facebook application.
