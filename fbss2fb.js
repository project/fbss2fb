// React when the form is submitted.
// Use this selector instead of #facebook_status_box in case more than one form exists on the page.
// Use $(context).find() instead of $() to avoid binding this behavior multiple times.

Drupal.behaviors.fbss2fb = function(context) {
	
  $(context).find('#facebook_status_replace').bind('ahah_success', function() {
    var fbss2fb_status = $(this).find('.facebook_status_text').val();
    // Get the FB dialog data from the callback page and pass it to the fb_stream module.
    // Only call the ajax request if the Facebook box is checked.
    if ($('#edit-fbss2fb-publish:checked').val() !== undefined) {
      $.post(Drupal.settings.basePath + 'fbss2fb/js/' + fbss2fb_status, FB_Stream.stream_publish);
    }  
  });

}